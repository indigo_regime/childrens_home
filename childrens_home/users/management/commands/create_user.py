# This command will allow us to create normal user for this system
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.core import exceptions
from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError
from django.db import IntegrityError


class Command(BaseCommand):
    help = 'Creates a normal user that has default permissons.'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.UserModel = get_user_model()
        self.username_field = self.UserModel._meta.get_field(
            self.UserModel.USERNAME_FIELD
        )

    def add_arguments(self, parser):
        # optional arguments
        parser.add_argument(
            '--staff_status',
            type=bool,
            help='Activates the user to have staff status active, this allows the user to login into the admin section.'
        )
        parser.add_argument(
            '--superuser',
            type=bool,
            help='Enables the newly created user to be a super_user.'
        )

    def handle(self, *args, **options):
        user_data = {}
        user_fields = [
            'username','password',
            'email','first_name',
            'last_name'
        ]
        if options.get('staff_status') or options.get('superuser'):
            staff_status = options['staff_status']
            super_user = options.get('superuser') # override everything to true
            if super_user == True:
                call_command('createsuperuser')
                return
            else:
                if staff_status == True:
                    user_data['is_staff'] = True
                else:
                    user_fields = user_fields

        user_model_meta = self.UserModel._meta

        for field in user_fields:
            field = user_model_meta.get_field(field)
            field_value = self.get_input_data(field)
            user_data[field.name]=field_value
            print(field_value)
        try:
            self.create_user(**user_data)
            self.stdout.write(self.style.SUCCESS('--Successfull operation--\n\tUser Created'))
        except Exception as e:
            self.stdout.write(self.style.ERROR('{}'.format(e)))

    def get_input_data(self, field):
        """
        Override this method if you want to customize data inputs or
        validation exceptions.
        """
        message = "Enter {}:".format(field.name)
        raw_value = input(message)
        try:
            val = field.clean(raw_value, None)
        except exceptions.ValidationError as e:
            self.stderr.write("Error: %s" % "; ".join(e.messages))
            val = None

        return val

    def create_user(self, **kwargs):
        try:
            User.objects.create_user(
                **kwargs
            )
        except IntegrityError :
            raise 